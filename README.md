# PAIN Website

This project allows key parts of the PAIN website to be edited without the need for backend access. The website will pull data from the most recent version of this project, to display. This makes the website much faster to edit w/o coding knowledge.
